# Agenda
- Basics of Python programming.
    - Variables: declaration, types
    - Get input from user
    - conditions
    - loops
- File operations 
- usage of Visual Code IDE for development of python\
    - installation
    - extension
    - auto completion
    - running 
    - debugging
    - 
- walk through of one real-time project

# References/ furthur reading
- https://www.tutorialspoint.com/python/index.htm
- https://www.w3schools.com/python/
- udemy.com